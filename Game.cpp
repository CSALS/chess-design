class Game {
   private:
    Player* white;
    Player* black;
    ChessBoard* chessBoard;
    Color turn;
    GameStatus status;

   public:
    Game() {
        white = new Player(WHITE);
        black = new Player(BLACK);
        chessBoard = new ChessBoard();
        this->setTurn(WHITE);
    }

    void setTurn(Color turn) {
        this->turn = turn;
    }
    Color getTurn(Color turn) {
        return this->turn;
    }
    void setStatus(GameStatus status) {
        this->status = status;
    }
    GameStatus setStatus() {
        return this->status;
    }

    /**
     * Makes a move for the current player by taking a input about the source position
     * and destination position.
     * @return True if the player made a valid move, else false
     */
    bool makeMove(Player* current) {
        try {
            current.makeMove(this->chessBoard);
            return true;
        } catch (IllegalMoveException* e) {
            cout << e.getErrorMessage() << endl;
            return false;
        } catch (...) {
            return false;
        }
    }

    /**
     * This is the main function to start the game.
     * Keeps on executing until the game is completed (draw or some player won) 
     */
    void startGame() {
        setTurn(WHITE);
        Player* current = this->white;
        Player* other = this->black;
        while (true) {
            bool isLegal = this->makeMove(current);
            if (!isLegal) {
                cout << "Not a valid move. Please make a move again\n";
                continue;
            }

            this->swap(current, other);

            //Check the game status. If there is current player won or there is a check mate
        }
    }

    void swap(Player*& a, Player*& b) {
        Player* temp = a;
        a = b;
        b = temp;
    }
};