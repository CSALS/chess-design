#include "Piece.cpp"

class ChessBoard {
   private:
    vector<vector<Piece*>> board;

    ChessBoard() {
        board.resize(8, vector<Piece*>(8, NULL));
        this->initializeBoard();
    }
    /**
    * This function initializes the board with the initial positions of pieces in a chess board
    */
    void initializeBoard() {
    }
};