#include <bits/stdc++.h>

#include "Position.cpp"
#include "enums.cpp"
using namespace std;

//Abstract class for a chess piece
class Piece {
   private:
    Position* currentPosition;
    Color color;
    State state;

   public:
    Piece(Color color, Position* position) {
        setPosition(position);
        setColor(color);
        setKilled(false);
    }

    void setColor(Color color) {
        this->color = color;
    }

    bool getColor() {
        return this->color;
    }

    void setState(State state) {
        this->state = state;
    }

    bool getState() {
        return this->state;
    }

    void setPosition(Position* position) {
        this->currentPosition = position;
    }

    Position* getPosition() {
        return this->currentPosition;
    }

    //This is an abstract function which will be defined in all the classes which inherit this class.
    virtual void move(Position* destination, Piece* destinationPiece) = 0;

    /**
     * Moves the current piece to the destination and also kills the piece at the destination
     * @param destination is the destination position
     * @param destinationPiece is the destination piece to be killed
     * @throw IllegalMoveException if trying to kill same color piece
     */
    void killPiece(Position* destination, Piece* destinationPiece) {
        //If no piece to kill
        if (destinationPiece == NULL) {
            cout << "Moved the piece to the destination\n";
            this->setPosition(destination);
        }

        //If same color piece present at destination
        if (destinationPiece->getColor() == this->getColor()) {
            throw new IllegalMoveException("Can't kill same color piece");
        }

        //Kill the opposite color piece
        destinationPiece->setPosition(NULL);
        destinationPiece->setState(DEAD);
        cout << "Killed the destination piece\n";
    }
};

class IllegalMoveException {
   private:
    string errorMessage;

   public:
    IllegalMoveException(string errorMessage) {
        this->errorMessage = errorMessage;
    }

    string getErrorMessage() {
        return this->errorMessage;
    }
};