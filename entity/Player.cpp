class Player {
   private:
    Color color;

   public:
    Player(Color color) {
        this->color = color;
    }

    /**
     * Makes a move in the board by taking input from the user
     */
    void makeMove(ChessBoard*& chessBoard) {
        //Take input from the user regarding the source and destination
        Position* source = NULL;
        Position* destination = NULL;
        cout << "Enter source coordinates\n";
        int sourceX, sourceY;
        cin >> sourceX >> sourceY;
        source = new Position(sourceX, sourceY);
        cout << "Enter destination coordinates\n";
        int destinationX, destinationY;
        cin >> destinationX >> destinationY;
        destination = new Position(destinationX, destinationY);

        //Check if source is valid

        //Call move() in Piece class
    }
}