#include "../Piece.cpp"
#include "../Position.cpp"

class Knight : public Piece {
   public:
    Knight(Color color, Position* position) : Piece(color, position) {}

    /**
     * This function first checks if it is a valid position and also if it valid move
     * for the knight.
     * @param destination is the destination position which the user wants to move this knight to.
     * @param destinationPiece is the piece present at that destination position or null if no piece present
     * @throw IllegalMoveException if the move is an illegal move
     */
    void move(Position* destination, Piece* destinationPiece) {
        int destX = destination->getX();
        int destY = destination->getY();
        if (destX >= 8 || destX < 0 || destY >= 8 || destY < 0) {
            throw new IllegalMoveException("Invalid destination coordinates");
        }

        const int SIZE = 8;
        int rowOffest[SIZE] = {-2, -1, 1, 2, -2, -1, 1, 2};
        int colOffest[SIZE] = {1, 2, 2, 1, -1, -2, -2, -1};

        int currX = this->getPosition()->getX();
        int currY = this->getPosition()->getY();

        bool isValid = false;
        for (int i = 0; i < SIZE; ++i) {
            int newX = currX + rowOffest[i];
            int newY = currY + colOffest[i];
            if (newX >= 0 && newY >= 0 && newX < 8 && newY < 8 && newX == destX && newY == destY) {
                isValid = true;
                break;
            }
        }

        if (!isValid) {
            throw new IllegalMoveException("Knight can't move to that position");
        }

        try {
            killPiece(destination, destinationPiece);
        } catch (IllegalMoveException* e) {
            throw e;
        }
    }
};
